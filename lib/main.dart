import 'package:app/view/task_manager_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:package/package.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  GetIt.I.registerSingleton<ApplicationSettingsService>(ApplicationSettingsService());
  GetIt.I.registerSingleton<LanguageService>(LanguageService());
  GetIt.I.registerSingleton<UserService>(UserService());

  ApplicationSettingsService applicationSettings = GetIt.I.get<ApplicationSettingsService>();
  applicationSettings.setDefaultValue('lang', 'en');
  applicationSettings.setMainPage(() => const TaskManagerPage());
  applicationSettings.init().then((value) => runApp(const MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale _locale = Locale(GetIt.I.get<ApplicationSettingsService>().get('lang'));

  @override
  void initState() {
    super.initState();
    GetIt.I.get<LanguageService>().locale.listen((value){
      setState(() {
        _locale  = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test app',
      theme: ThemeData(
        primarySwatch: ApplicationColors.primary,
      ),
      home: const LoginPage(),
      localizationsDelegates: const [
        PackageLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: PackageLocalizations.supportedLocales,
      locale: _locale,
    );
  }
}