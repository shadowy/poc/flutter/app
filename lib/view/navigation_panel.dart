import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:package/package.dart';


class NavigationPanel extends StatefulWidget {
  const NavigationPanel({Key? key}) : super(key: key);

  @override
  _NavigationPanelState createState() {
    return _NavigationPanelState();
  }
}

class _NavigationPanelState extends State<NavigationPanel> {
  late StreamSubscription _userInformationListener;
  String userName = '';

  @override
  void initState() {
    super.initState();
    _userInformationListener = GetIt.I.get<UserService>().userInformation.listen(((event) {
      setState(() {
        userName = event == null ? '' : event.name;
      });
    }));
  }

  @override
  void dispose() {
    super.dispose();
    _userInformationListener.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color:  ApplicationColors.primary.shade400, //Theme.of(context).colorScheme.tertiary
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Image(image: AssetImage('assets/logo_avtologistika_rsz.png'), fit: BoxFit.scaleDown),
                Container(height: 2 * space,),
                Text(userName, style: const TextStyle(color: ApplicationColors.text, fontWeight: FontWeight.w900)),
                Container(height: space,),
                const Text('Drawer Header1', style: TextStyle(color: ApplicationColors.text)),
              ]
            ),
          ),
        ],
      ),
    );
  }
}