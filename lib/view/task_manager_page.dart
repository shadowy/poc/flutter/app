import 'package:app/view/navigation_panel.dart';
import 'package:flutter/material.dart';

class TaskManagerPage extends StatefulWidget {
  const TaskManagerPage({Key? key}) : super(key: key);

  @override
  _TaskManagerPageState createState() => _TaskManagerPageState();
}

class _TaskManagerPageState extends State<TaskManagerPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      appBar: AppBar(
        title: const Text('Осмотр'),
      ),
      drawer: const NavigationPanel(),
      body: const Center(
        child: Text('My Page!'),
      ),
    ));
  }
}
